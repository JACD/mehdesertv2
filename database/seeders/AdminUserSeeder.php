<?php

namespace Database\Seeders; 
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB; 
use App\Models\User;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Jhojie',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456'),
            'roles' => '1',
        ]);
    }
}
